package ltd.qnyijt.autoclick;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.accessibilityservice.GestureDescription;
import android.app.Service;
import android.content.Intent;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Handler;
import android.os.IBinder;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AutoClickWeChatLoginAccessibilityService extends AccessibilityService {
    public AutoClickWeChatLoginAccessibilityService() {
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        AccessibilityNodeInfo nodeInfo = accessibilityEvent.getSource();
        if (nodeInfo == null) {
            return;
        }
        ArrayList<AccessibilityNodeInfo> list = new ArrayList<>();

        String text = getListItemNodeInfo(nodeInfo, list, "");

        // 先判断页面中是否有登录确认字样
        if (text.contains("登录确认")) {
            // 如果有登录确认，找到登录按钮
            AccessibilityNodeInfo loginButton = list.stream()
                    .filter(i -> i.getText() != null && "登录".equals(i.getText().toString()))
                    .findFirst()
                    .orElse(null);

            // 如果可以找到登录按钮，就进行点击
            if (null != loginButton) {
                Path mPath = new Path();
                Rect rect = new Rect();
                loginButton.getBoundsInScreen(rect);
                int x = (rect.left + rect.right) / 2;
                int y = (rect.top + rect.bottom) / 2;
                mPath.moveTo(x, y);
                new Handler().postDelayed(() -> {
                    dispatchGesture(
                            new GestureDescription.Builder()
                                    .addStroke(new GestureDescription
                                            .StrokeDescription(mPath, 0, 100))
                                    .build(),
                            null,
                            null);

                    Toast.makeText(getApplicationContext(),
                                    "登录按钮已点击",
                                    Toast.LENGTH_LONG).show();
                }, 500);

            }
        }

    }

    @Override
    public void onInterrupt() {

    }

    @Override
    public void onServiceConnected() {
        AccessibilityServiceInfo info = getServiceInfo();
        info.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED;

         info.packageNames = new String[]
                {"com.tencent.mm"};

        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_VISUAL;

        info.notificationTimeout = 100;

        this.setServiceInfo(info);
    }

    private String getListItemNodeInfo(AccessibilityNodeInfo source, List<AccessibilityNodeInfo> nodeInfoList, String text) {

        if (source == null) {
            return "";
        }
        if (source.getChildCount() == 0) {
            nodeInfoList.add(source);
            return text + source.getText();
        }

        nodeInfoList.add(source);
        for (int i = 0; i < source.getChildCount(); i++) {
            text = getListItemNodeInfo(source.getChild(i), nodeInfoList, text + source.getText());
        }
        return text;
    }

}